<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// public route
Auth::routes();
Route::get('/', 'IndexController@index')->name('index');
Route::get('/detail/{id}', 'IndexController@show')->name('detail');
Route::post('/comment', 'IndexController@store')->name('comment');
Route::post('/search', 'IndexController@index')->name('search');

//dashboard
Route::get('/home', 'HomeController@index')->name('home');

// route movie
Route::get('/movie', 'MovieController@index')->name('movie');
Route::get('/movie/create', 'MovieController@create')->name('movie_create');
Route::post('/movie/store', 'MovieController@store')->name('movie_store');
Route::get('/movie/edit={id}', 'MovieController@edit')->name('movie_edit');
Route::patch('/movie/update={id}', 'MovieController@update')->name('movie_update');
Route::delete('/movie/delete={id}', 'MovieController@destroy')->name('movie_delete');

//logout
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
