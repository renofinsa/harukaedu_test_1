<?php

use Illuminate\Database\Seeder;
use App\Movie;
class MovieSeedTables extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Movie::create([
            'title'         => 'Maleficent: Mistress of Evil',
            'duration'      => '118',
            'release'       => '20191018',
            'stars'         => 'Angelina Jolie, Elle Fanning, Harris Dickinson',
            'director'      => 'Joachim Rønning',
            'writers'       => 'Micah Fitzerman-Blue, Noah Harpster',
            'description'   => 'Maleficent and her goddaughter Aurora begin to question the complex family ties that bind them as they are pulled in different directions by impending nuptials, unexpected allies, and dark new forces at play.',
            'video'         => 'n0OFH4xpPr4',
            'genre'         => 'Adventure, Family, Fantasy',
            'poster'        => 'maleficent.jpg',
            'ratting'       => '7'
        ]);

        Movie::create([
            'title'         => 'Zombieland: Double Tap',
            'duration'      => '99',
            'release'       => '20191018',
            'stars'         => 'Woody Harrelson, Jesse Eisenberg, Emma Stone',
            'director'      => 'Ruben Fleischer',
            'writers'       => 'Dave Callaham, Rhett Reese',
            'description'   => 'Columbus, Tallahassee, Wichita, and Little Rock move to the American heartland as they face off against evolved zombies, fellow survivors, and the growing pains of the snarky makeshift family.',
            'video'         => 'ZlW9yhUKlkQ',
            'genre'         => 'Action, Comedy, Horror',
            'poster'        => 'zombieland.jpg',
            'ratting'       => '8'
        ]);

        Movie::create([
            'title'         => 'Jojo Rabbit',
            'duration'      => '108',
            'release'       => '20191018',
            'stars'         => 'Roman Griffin Davis, Thomasin McKenzie, Scarlett Johansson',
            'director'      => 'Taika Waititi',
            'writers'       => 'Christine Leunens, Taika Waititi',
            'description'   => 'A young boy in Hitler army finds out his mother is hiding a Jewish girl in their home.',
            'video'         => 'tL4McUzXfFI',
            'genre'         => 'Comedy, Drama, War',
            'poster'        => 'jojo_rabbit.jpg',
            'ratting'       => '7'
        ]);
    }
}
