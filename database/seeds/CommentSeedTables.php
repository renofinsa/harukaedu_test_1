<?php

use Illuminate\Database\Seeder;
use App\Comment;
class CommentSeedTables extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Comment::create([
            'movie_id'     => 1,
            'name'         => 'Reno',
            'comment'      => 'Sungguh sangat dinantikan.',
        ]);

        Comment::create([
            'movie_id'     => 1,
            'name'         => 'Finsa',
            'comment'      => 'Masih sibuk belum bisa nonton.',
        ]);

        Comment::create([
            'movie_id'     => 2,
            'name'         => 'Kaka',
            'comment'      => 'Wahh seru banget filmnya.',
        ]);

        Comment::create([
            'movie_id'     => 2,
            'name'         => 'Bunda',
            'comment'      => 'Seremmmm.',
        ]);

        Comment::create([
            'movie_id'     => 2,
            'name'         => 'Cha',
            'comment'      => 'Kyaa ada Emma.',
        ]);

        Comment::create([
            'movie_id'     => 3,
            'name'         => 'Eto',
            'comment'      => 'Ini film hitler?.',
        ]);
    }
}
