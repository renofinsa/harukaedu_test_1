<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // insert user baru
        $data = new User();
        $data->name     = 'admin';
        $data->email    = 'me@admin.com';
        $data->password = bcrypt('admin');
        $data->save();
    }
}
