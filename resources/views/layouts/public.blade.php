<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="harukaEDU test 1, build web movie MVP concept with Laravel 6.0 and MySQL">
  <meta name="author" content="renofinsa">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title> @yield('title') </title>
  @yield('style')
</head>

<body id="page-top">
  @yield('content')

  @yield('script')
</body>

</html>
