@if(\Session::has('alert'))
<div class="alert alert-success" role="alert" style="text-align:right;">
        <p>{{\Session::get('alert')}}</p>
</div>
@endif

@if(\Session::has('alertWarning'))
<div class="alert alert-danger" role="alertWarning" style="text-align:right;">
        <p>{{\Session::get('alertWarning')}}</p>
</div>
@endif
