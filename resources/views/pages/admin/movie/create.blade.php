@extends('layouts.app')

@section('title','Movi3 - Dashboard')

@section('style')
    @include('partials._style')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('movie')}}">Movie</a></li>
            <li class="breadcrumb-item active" aria-current="page">Create</li>
        </ol>
    </nav>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
        </div>
        <div class="card-body">
            <form action="{{route('movie_store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="_method" value="POST">
                <div class="form-group">
                      <label for="">Movie Title</label>
                      <input type="text" class="form-control form-control-user @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" placeholder="Enter Title Movie..." required autocomplete="title" autofocus>
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
                <div class="form-group row">
                      <div class="col-md-4">
                            <label for="">Duration (with minute)</label>
                            <input type="number" min="1" max="1000" class="form-control form-control-user @error('duration') is-invalid @enderror" name="duration" value="{{ old('duration') }}" placeholder="Enter Duration Movie..." required autocomplete="duration" autofocus>
                              @error('duration')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                      </div>
                      <div class="col-md-4">
                            <label for="">Release</label>
                            <input type="date" class="form-control form-control-user @error('release') is-invalid @enderror" name="release" value="{{ old('release') }}" placeholder="Enter Release Movie..." required autocomplete="release" autofocus>
                              @error('release')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                      </div>
                      <div class="col-md-4">
                            <label for="">Ratting</label>
                            <input type="number" min="1" max="10" class="form-control form-control-user @error('ratting') is-invalid @enderror" name="ratting" value="{{ old('ratting') }}" placeholder="Enter Ratting..." required autocomplete="ratting" autofocus>
                                @error('ratting')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                      </div>
                </div>
                <div class="form-group row">
                      <div class="col-md-6">
                            <label for="">Director</label>
                            <input type="text" class="form-control form-control-user @error('director') is-invalid @enderror" name="director" value="{{ old('director') }}" placeholder="Enter Director..." required autocomplete="director" autofocus>
                              @error('director')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                      </div>
                      <div class="col-md-6">
                            <label for="">Writers</label>
                            <input type="text" class="form-control form-control-user @error('writers') is-invalid @enderror" name="writers" value="{{ old('writers') }}" placeholder="Enter Writers Movie..." required autocomplete="writers" autofocus>
                              @error('writers')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                      </div>
                </div>
                <div class="form-group">
                    <label for="">Genre</label>
                    <input type="text" class="form-control form-control-user @error('genre') is-invalid @enderror" name="genre" value="{{ old('genre') }}" placeholder="Enter Genre Movie..." required autocomplete="genre" autofocus>
                    @error('genre')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Stars</label>
                        <input type="text" class="form-control form-control-user @error('stars') is-invalid @enderror" name="stars" value="{{ old('stars') }}" placeholder="Enter Stars..." required autocomplete="stars" autofocus>
                        @error('stars')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
                <div class="form-group">
                      <label for="">Description</label>
                      <textarea cols="30" rows="10" class="form-control form-control-user @error('description') is-invalid @enderror" name="description" placeholder="Enter Movie Description..." required autocomplete="description" autofocus>{{ old('description') }}</textarea>
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
                <div class="form-group row">
                      <div class="col-md-4">
                            <label for="">Upload Poster</label>
                            <input type="file" class="form-control form-control-user @error('poster') is-invalid @enderror" name="poster" value="{{ old('poster') }}" required autofocus>
                            @error('poster')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                      </div>
                      <div class="col-md-8">
                            <label for="">Trailer Movie Link (Should Embed Id in Youtube) Ex. n0OFH4xpPr4</label>
                            <input type="text" class="form-control form-control-user @error('video') is-invalid @enderror" name="video" value="{{ old('video') }}" placeholder="Enter Trailer Movie Link..." required autocomplete="video" autofocus>
                            @error('video')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                      </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary form-control">Submit</button>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('script')
    @include('partials._script')
@endsection
