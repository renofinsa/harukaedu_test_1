@extends('layouts.app')

@section('title','Movi3 - Dashboard')

@section('style')
    @include('partials.style')
@endsection

@section('content')
<!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Movie</li>
        </ol>
    </nav>

    @include('partials.alert')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a href="{{route('movie_create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" style="float:right"><i class="fas fa-plus fa-sm text-white-50"></i> Add Movie</a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th width="50">No</th>
                  <th>Name</th>
                  <th width="150">Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>
                <?php $no = 0 ?>
                @foreach ($data as $item)
                <?php $no++ ?>
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$item->title}}</td>
                  <td>
                    <a href="{{route('movie_edit',$item->id)}}" class="btn btn-success"><i class="fas fa-pencil-alt fa-sm"></i></a>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete{{$item->id}}">
                        <i class="fas fa-trash fa-sm"></i>
                    </button>
                  </td>
                </tr>

                <!-- Delete  -->
                <div class="modal fade" id="delete{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Delete Movie Id #{{$item->id}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Click Remove if you want delete this file ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <form action="{{route('movie_delete',$item->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class="btn btn-primary">Remove</button>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>

                @endforeach

              </tbody>
            </table>
          </div>
        </div>
    </div>

@endsection

@section('script')
    @include('partials.script')
@endsection
