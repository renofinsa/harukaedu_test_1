@extends('layouts.public')

@section('title','MovieSite')

@section('style')
    @include('partials._style')
@endsection

@section('content')
    @include('partials.alert')
    <div class="container" style="margin-top: 50px;">
        <div class="row" style="margin-top: 30px; margin-bottom: 50px">
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img style="height: 80%; width: 100%" src="../storage/upload/poster/{{$data->poster}}" class="card-img-top" alt="poster">
                    </div>
                    <div class="col-md-8">
                    <iframe width="100%" height="80%" src="https://www.youtube.com/embed/{{$data->video}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="card-body" style="margin-top: -100px">
                    <h5 class="card-title">{{$data->title}} <span style="font-size: 16px">({{date('Y', strtotime($data->release))}})</span></h5>
                    <h6> {{$data->duration}} min | {{$data->genre}} | {{date('d M Y', strtotime($data->release))}} </h6>
                    <hr>
                    <p class="card-text">{!!$data->description!!}</p>
                    <p> <b>Director :</b> {!!$data->director!!} </p>
                    <p> <b>Writers :</b> {!!$data->writers!!} </p>
                    <p> <b>Stars :</b> {!!$data->stars!!}</p>
                  <a href="{{route('index')}}" class="btn btn-danger" style="float:right"><i class="fas fa-angle-left fa-sm"></i> Back</a>
                </div>
                <div class="card-footer">
                    <h5>Comments</h5>
                    <form action="{{route('comment')}}" method="post">
                         @csrf
                        <input type="hidden" name="_method" value="POST">
                        <input type="hidden" name="movie_id" value="{{$data->id}}">
                        <div class="form-group">
                            <input style="width:200px" type="text" class="form-control form-control-user @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Enter Name..." required autocomplete="name">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group">
                              <textarea cols="30" rows="5" class="form-control form-control-user @error('comment') is-invalid @enderror" name="comment" placeholder="Enter Comment..." required autocomplete="comment">{{ old('comment') }}</textarea>
                                @error('comment')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary form-control">Submit</button>
                        </div>
                    </form>
                    <hr>
                    @foreach ($comment as $item)
                    <div class="form-group card">
                        <div class="card-body">
                            <h5>{{$item->name}} <span style="font-size:14px; float:right">( {{$item->created_at->diffForHumans()}} )</span></h5>
                            <p>{{$item->comment}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    @include('partials._script')
@endsection
