@extends('layouts.public')

@section('title','MovieSite')

@section('style')
    @include('partials.style')
@endsection

@section('content')
    <div class="container" style="margin-top: 50px; margin-bottom: 50px">
        <div class="row">
            <form action="{{route('search')}}" method="post" class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                @csrf
                <div class="input-group">
                <input name="key" type="text" style="width:250px" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit">
                    <i class="fas fa-search fa-sm"></i>
                    </button>
                </div>
                </div>
            </form>
            <a href="{{route('login')}}" class="btn btn-danger" style="float:right"><i class="fas fa-lock fa-sm"></i> Login</a>
        </div>
        <div class="row" style="margin-top: 30px">
            @foreach ($data as $item)

            <div class="col-md-4">
                <div class="card" style="width: 21rem; height: 10em; ">
                    <img src="storage/upload/poster/{{$item->poster}}" class="card-img-top" alt="poster">
                    <div class="card-body" style="border: 1px solid black; height:15em">
                        <h5 class="card-title">{!!Str::limit($item->title, 28, ' ...')!!} <span style="font-size: 16px">({{date('Y', strtotime($item->release))}})</span></h5>
                        <p class="card-text">{!!Str::limit($item->description, 100, ' ...')!!}</p>
                        <a href="{{route('detail',$item->id)}}" class="btn btn-primary" style="float:right">View More</a>
                    </div>
                </div>
            </div>

            @endforeach

        </div>
    </div>

@endsection

@section('script')
    @include('partials.script')
@endsection
