
## About Apps
Ini adalah contoh website movie seperti imdb.com. dibuat simple menggunakan bahasa PHP7++ dengan teknologi Laravel versi 6.0 dan MySQL.

Website ini berisi fitur pencarian berdasarkan judul pada homepage, dan memiliki halaman administrator / backoffice untuk membuat data movienya.

## Requirement Install
- Composer.
- MySQL.
- MySQLWorkbrench (untuk melihat database secara visual).
- PHP 7.2++ (pembuatan contoh pada program ini menggunakan ver 7.3).
- Browser (rekomendasi menggunakan chrome).

## How To Run
- Buka Terminal, tentukan directory / folder tujuan.
- Lalu ketik, https://gitlab.com/renofinsa/harukaedu_test_1.git.
- Tunggu sampai proses selesai, lalu masuk ke directory repo yang sudah diclon.
- Buka terminal lalu kelik composer install, (tunggu sampai proses selesai).
- Buka project, lalu buka file .env, (kalau tidak ada, kamu bisa copy paste dari env.example).
- Sebelumnya kamu harus install Mysql dan setup username, password serta database yang akan digunakan.
- Setelah itu kamu setup/ ganti pada bagian database, username dan password sesuai dengan yang kamu setup.
- Setelah semua tersimpang, maka kamu dapat klik printah "php artisan migrate" pada terminal, dilanjutkan dengan perintah "php artisan db:seed" untuk replace database yang sudah ditentukan / dibuat pada seeder.
- Proses selesai dan kamu bisa langsung ketik printah "php artisan ser" untuk menjalankan aplikasi.
- Lalu akan muncul alamat aplikasi yang akan dapat diakses via browser kamu, biasanya laravel menggunakan port 8080, seperti ini "localhost:8080"
- Aplikasi siap digunakan.

## Akses Login
- Untuk saat ini aplikasi hanya memiliki 1 akses login :
    - Email : me@admin.com
    - Password : admin
