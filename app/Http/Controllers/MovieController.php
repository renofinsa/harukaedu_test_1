<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;
use App\Movie;
use App\RelationGenreMovie;
class MovieController extends Controller
{
    // middleware
    public function __construct()
    {
        $this->middleware('auth');
    }

    // menu movie
    public function index()
    {
        $data = Movie::orderBy('created_at','desc')->get();
        return view('pages.admin.movie.index',compact('data'));
    }

    // melihat data pada form yang akan diinput
    public function create()
    {
        return view('pages.admin.movie.create');
    }

    // proses input data baru
    public function store(Request $request)
    {
        // validation
        $a = $request->validate([
            'title' => ['required', 'string','min:1','max:100'],
            'duration' => ['required', 'string'],
            'release' => ['required', 'string'],
            'stars' => ['required', 'string'],
            'director' => ['required', 'string'],
            'writers' => ['required', 'string'],
            'description' => ['required', 'string'],
            'video' => ['required', 'string'],
            'genre' => ['required', 'string','min:2'],
            'ratting' => ['required', 'string'],
        ]);

        // upload file handle
        if($request->hasFile('poster')){
            $filenameWithExt = $request->file('poster')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('poster')->getClientOriginalExtension();
            $file_store_name = $filename.'_'.time().'.'.$extension;
            $path = $request->file('poster')->storeAs('public/upload/poster', $file_store_name);
        }else{
            $file_store_name = 'null.png';
        }

        // insert handle
        $data = new Movie();
        $data->title        = $a['title'];
        $data->duration     = $a['duration'];
        $data->release      = $a['release'];
        $data->stars        = $a['stars'];
        $data->director     = $a['director'];
        $data->writers      = $a['writers'];
        $data->description  = $a['description'];
        $data->video        = $a['video'];
        $data->genre        = $a['genre'];
        $data->ratting      = $a['ratting'];
        $data->poster       = $file_store_name;
        $data->save();
        return redirect(route('movie'))->with('alert','Success!');
    }

    // melihat data pada form yang akan diupdate
    public function edit($id)
    {
        $data = movie::find($id);
        return view('pages.admin.movie.update',compact('data'));
    }

    // update movie
    public function update(Request $request, $id)
    {
       // validation
       $a = $request->validate([
        'title' => ['required', 'string','min:1','max:100'],
        'duration' => ['required', 'string'],
        'release' => ['required', 'string'],
        'stars' => ['required', 'string'],
        'director' => ['required', 'string'],
        'writers' => ['required', 'string'],
        'description' => ['required', 'string'],
        'video' => ['required', 'string'],
        'genre' => ['required', 'string','min:2'],
        'ratting' => ['required', 'string'],
        ]);

        // upload file handle
        if($request->hasFile('poster')){
            $filenameWithExt = $request->file('poster')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('poster')->getClientOriginalExtension();
            $file_store_name = $filename.'_'.time().'.'.$extension;
            $path = $request->file('poster')->storeAs('public/upload/poster', $file_store_name);
            unlink('storage/upload/poster/'.$request->get('old_poster').'');
        }else{
            $file_store_name = $request->get('old_poster');
        }

        // insert handle
        $data = Movie::find($id);
        $data->title        = $a['title'];
        $data->duration     = $a['duration'];
        $data->release      = $a['release'];
        $data->stars        = $a['stars'];
        $data->director     = $a['director'];
        $data->writers      = $a['writers'];
        $data->description  = $a['description'];
        $data->video        = $a['video'];
        $data->genre        = $a['genre'];
        $data->ratting      = $a['ratting'];
        $data->poster       = $file_store_name;
        $data->save();
        return redirect(route('movie'))->with('alert','Success!');
    }

   // menghapus movie
    public function destroy($id)
    {
        $data = Movie::find($id);
        unlink('storage/upload/poster/'.$data->poster.'');
        $data->delete();
        return redirect(route('movie'))->with('alert','Deleted Success!');
    }
}
