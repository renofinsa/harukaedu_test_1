<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\Comment;
class IndexController extends Controller
{
    // homepage / menu utama | dapat lagsung mencari movie berdasarkan judul
    public function index(Request $request)
    {
        $key = $request->get('key');
        $data = Movie::where('title','like','%'.$key.'%')->latest()->get();
        return view('pages.index',compact('data'));
    }

    // submit comment pada bagian detail movie
    public function store(Request $request)
    {
        $id = $request->get('movie_id');
        $a = $request->validate([
        'name' => ['required', 'string','min:2','max:50'],
        'comment' => ['required', 'string','min:10','max:150'],
        ]);

        $data = new Comment();
        $data->movie_id = $id;
        $data->comment = $a['comment'];
        $data->name = $a['name'];
        $data->save();
        return redirect(route('detail',$id))->with('alert','Comments Success!');
    }

    // detail page movie
    public function show($id)
    {
        $comment = Comment::orderBy('created_at','desc')->where('movie_id',$id)->get();
        $data = Movie::find($id);
        return view('pages.detail',compact('data','comment'));
    }

}
