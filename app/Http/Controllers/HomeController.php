<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Movie;
class HomeController extends Controller
{
    // middleware
    public function __construct()
    {
        $this->middleware('auth');
    }

    // admin dashboard
    public function index()
    {
        $comment = Comment::count();
        $movie = Movie::count();
        return view('pages.admin.index',compact('comment','movie'));
    }
}
